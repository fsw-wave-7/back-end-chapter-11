const AuthController = require('../controllers/api/authController');
const { User } = require('../models');

const mockRequest = (body = {}) => ({ body });
const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

jest.mock('../models/index.js', () => {
  return {
    User: {
      authenticate: jest.fn(() => Promise.reject('error')),
      register: jest.fn(() => Promise.reject({
        err: {
          errors: [{
            message: 'error'
          }]
        }
      }))
    }
  };
});

describe('auth', () => {
  let mRes;
  let mNext;
  let auth;
  beforeEach(() => {
    mRes = mockResponse(),
    mNext = jest.fn();
    auth = new AuthController();
  });

  test('login', async () => {
    const mReq = mockRequest({
      email: 'us3r22222222@gmail.com',
      password: 'us3r22222222'
    });
    await auth.login(mReq, mRes, mNext);
    expect(User.authenticate).toHaveBeenCalledTimes(1);
  });
  test('register', async () => {
    const mReq = mockRequest({
      username: 'us3r22222222',
      email: 'us3r22222222@gmail.com',
      password: 'us3r22222222'
    });
    await auth.register(mReq, mRes, mNext);
    expect(User.register).toHaveBeenCalled();
  });
});
