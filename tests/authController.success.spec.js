const AuthController = require('../controllers/api/authController');
const { User } = require('../models');

const mockRequest = (body = {}) => ({ body });
const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

let responseLogin = { id: 1, username: 'user1', accessToken: 'token' };
let responseRegister = {
  data: {
    id: 1,
    username: 'us3r22222222a',
    email: 'us3r222222a22@gmail.com',
  },
  meta: {
    code: 201,
    message: 'data berhasil disimpan',
  },
};
jest.mock('../models/index.js', () => {
  return {
    User: {
      authenticate: jest.fn(() => Promise.resolve(responseLogin)),
      register: jest.fn(() => Promise.resolve(responseRegister)),
    },
  };
});

describe('auth', () => {
  let mRes;
  let mNext;
  let auth;
  beforeEach(() => {
    (mRes = mockResponse()), (mNext = jest.fn());
    auth = new AuthController();
  });

  test('login', async () => {
    const mReq = mockRequest({
      email: 'us3r22222222@gmail.com',
      password: 'us3r22222222',
    });
    await auth.login(mReq, mRes, mNext);
    expect(User.authenticate).toHaveBeenCalledTimes(1);
    expect(mRes.status).toBeCalledWith(200);
  });
  test('register', async () => {
    const mReq = mockRequest({
      username: 'us3r22222222',
      email: 'us3r22222222@gmail.com',
      password: 'us3r22222222',
    });
    await auth.register(mReq, mRes, mNext);
    expect(User.register).toHaveBeenCalled();
    expect(mRes.status).toBeCalledWith(201);

    // bug
    // expect(mRes.json).toBeCalledWith(responseRegister);
  });
});
