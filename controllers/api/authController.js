const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');

const { User } = require('../../models');
const { successResponse } = require('../../helpers/response');
// const { SMTPClient } = require('emailjs');

function format(user) {
  const { id, username, generateToken } = user;
  return {
    id,
    username,
    accessToken: generateToken(),
  };
}
class AuthController {
  register = (req, res) => {
    User.register(req.body)
      .then((user) => {
        res.json(
          successResponse(res, 201, {
            id: user.id,
            username: user.username,
            email: user.email,
          })
        );
      })
      .catch((err) => {
        res.json(
          successResponse(res, 422, null, { message: err.errors[0].message })
        );
      });
  };

  login = (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        res.status(200).json(format(user));
      })
      .catch((err) => {
        console.log(err);

        res.json(successResponse(res, 401, null, { message: err }));
      });
  };

  logout = (req, res) => {
    res.clearCookie('loginData');
    res.json(successResponse(res, 200));
  };

  forgot = (req, res) => {
    const token = Math.random().toString(36).substr(2, 11);

    User.findOne({ where: { email: req.body.email } }).then((user) => {
      if (!user) {
        res.json(
          successResponse(res, 422, null, {
            message: 'No account with that email address exists.',
          })
        );
      }
      User.update(
        {
          resetPasswordToken: token,
          resetPasswordExpires: Date.now() + 3600000,
        },
        { where: { email: req.body.email } }
      );
      // const client = new SMTPClient({
      //   user: 'nitipyambah@gmail.com',
      //   password: '12erqw34',
      //   host: 'smtp.gmail.com',
      //   ssl: true,
      // });

      // const message = {
      //   text: 'i hope this works',
      //   from: 'you <username@your-email.com>',
      //   to: 'someone <someone@your-email.com>, another <another@your-email.com>',
      //   cc: 'else <else@your-email.com>',
      //   subject: 'testing emailjs',
      //   attachment: [
      //     { data: '<html>i <i>hope</i> this works!</html>', alternative: true },
      //   ],
      // };

      // //send the message and get a callback with an error or details of the message that was sent
      // client.send(message, function (err, message) {
      //   console.log(err || message);
      //   res.json({message});
      // });

      var smtpTrans = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'nitipyambah@gmail.com',
          pass: '12erqw34',
        },
      });
      var mailOptions = {
        to: user.email,
        from: 'FSW WAVE 7',
        subject: 'FSW WAVE 7 Password Reset',
        text: `You are receiving this because you (or someone else) have requested the reset of the password for your account.

          Please click on the following link, or paste this into your browser to complete the process:
          https://front-end-chapter-eleven.herokuapp.com/reset/${token} 
          
          If you did not request this, please ignore this email and your password will remain unchanged.`,
      };

      smtpTrans.sendMail(mailOptions, () => {
        res.json(
          successResponse(res, 200, null, {
            message: 'Email sent',
            token,
          })
        );
        console.log('sent');
      });
    });
  };

  getReset = (req, res) => {
    User.findOne({
      where: {
        resetPasswordToken: req.params.token,
        // resetPasswordExpires: { $gt: new Date().toLocaleString('id-ID') },
      },
    }).then((user) => {
      console.log(user);

      if (!user) {
        res.json(
          successResponse(res, 401, null, {
            message: 'Password reset token is invalid or has expired.',
          })
        );
      } else {
        res.json(
          successResponse(res, 200, null, { message: 'masukkan password baru' })
        );
      }
    });
  };

  postReset = (req, res) => {
    User.findOne({
      where: {
        resetPasswordToken: req.params.token,
      },
    }).then((user) => {
      if (!user) {
        res.json(
          successResponse(res, 401, null, {
            message: 'Password reset token is invalid or has expired.',
          })
        );
      }
      User.update(
        {
          resetPasswordToken: undefined,
          resetPasswordExpires: undefined,
          password: bcrypt.hashSync(req.body.password, 10),
        },
        { where: { resetPasswordToken: req.params.token } }
      )
        .then((user) => {
          res.json(successResponse(res, 200, user));
        })
        .catch((err) => {
          res.json(
            successResponse(res, 422, null, { message: err.errors[0].message })
          );
        });
    });
  };
}

module.exports = AuthController;
