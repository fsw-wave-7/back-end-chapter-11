const bcrypt = require('bcrypt');

const { User, City, Score } = require('../../models');
const { successResponse } = require('../../helpers/response');
const { cloudinary } = require('../../helpers/cloudinary');

class UserController {
  // USER
  getProfile = (req, res) => {
    const currentUser = req.user;
    User.findOne({
      where: { username: currentUser.username },
      include: [
        {
          model: City,
          as: 'city',
        },
        {
          model: Score,
          as: 'score',
        },
      ],
    })
      .then((user) => {
        res.json(
          successResponse(res, 200, {
            id: user.id,
            username: user.username,
            score: user.score !== null ? user.score.score : null,
            city:
              user.city !== null
                ? { id: user.city.id, name: user.city.name }
                : null,
            email: user.email,
            biodata: user.biodata,
            social_media_url: user.social_media_url,
            urlProfile: user.urlProfile,
          })
        );
      })
      .catch('fail');
  };

  updateProfile = (req, res) => {
    const currentUser = req.user;
    const {
      username, city_id, email, biodata, social_media_url
    } = req.body;
    User.update(
      {
        username,
        city_id,
        email,
        biodata,
        social_media_url,
      },
      {
        where: { username: currentUser.username },
        include: [
          {
            model: City,
            as: 'city',
          },
          {
            model: Score,
            as: 'score',
          },
        ],
      }
    )
      .then((user) => {
        res.json(successResponse(res, 200, user));
      })
      .catch((err) => {
        res.json(
          successResponse(res, 422, null, { message: err.errors[0].message })
        );
      });
  };

  updateImage = async (req, res) => {
    const currentUser = req.user;
    const { dataImage } = req.body;
    const uploadResponse = await cloudinary.uploader
      .upload(dataImage, {
        upload_preset: 'dev_setups',
      })
      .catch((err) => err);
    User.update(
      {
        urlProfile: uploadResponse.url,
      },
      {
        where: { username: currentUser.username },
      }
    )
      .then((user) => {
        res.json(successResponse(res, 200, user));
      })
      .catch((err) => {
        res.json(successResponse(res, 422, null, { message: err }));
      });
  };

  getCity = (req, res) => {
    City.findAll()
      .then((cities) => {
        const city = cities.map((oneCity) => {
          return {
            id: oneCity.id,
            name: oneCity.name,
          };
        });
        res.json(successResponse(res, 200, city));
      })
      .catch('fail');
  };

  deleteUser = (req, res) => {
    User.destroy({
      where: {
        id: req.params.id,
      },
    }).then(() => {
      res.json(successResponse(res, 200, null));
    });
  };
}

module.exports = UserController;
